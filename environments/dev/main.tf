module "frontend_spa1" {
  source      = "../../modules/application/spa"
  app_name    = "SPA1 Development"
  callbacks   = ["https://dev-spa1.example.com/callback"]
  database_connection_id  = module.database_connection.database_connection_id
  google_connection_id    = module.google_connection.google_connection_id
}

module "frontend_spa2" {
  source      = "../../modules/application/spa"
  app_name    = "SPA2 Development"
  callbacks   = ["https://dev-spa2.example.com/callback"]
  database_connection_id  = module.database_connection.database_connection_id
  google_connection_id    = module.google_connection.google_connection_id
}

module "web_app" {
  source      = "../../modules/application/webapp"
  app_name    = "Backend Development"
  callbacks   = ["https://dev-backend.example.com/callback"]
}

module "backend_api" {
  source      = "../../modules/api"
  api_name    = "Backend API Development"
  api_identifier  = "https://dev-api.example.com"
}

module "database_connection" {
  source = "../../modules/authentication/database"
}

module "google_connection" {
  source = "../../modules/authentication/social"
}

module "auth0_actions" {
  source = "../../modules/action"
}
