variable "auth0_domain" {
    description = "Auth0 Domain"
    type = string
}

variable "auth0_client_id" {
    description = "Auth0 Client ID"
    type = string
}

variable "auth0_client_secret" {
    description = "Auth0 Client Secret"
    type = string
}

variable "auth0_debug" {
    description = "Auth0 Debug"
    type = bool
}