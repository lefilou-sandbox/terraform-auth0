terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0" # Refer to docs for latest version
    }
  }
}

resource "auth0_connection" "google" {
  name     = "google-connection"
  strategy = "google-oauth2"

  options {
    client_id     = "<client-id>"
    client_secret = "<client-secret>"
    scopes        = ["email", "profile", "gmail", "youtube"]  
  }

}

output "google_connection_id" {
  description = "ID of the Auth0 Google connection"
  value       = auth0_connection.google.id
}