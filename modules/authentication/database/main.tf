terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0" # Refer to docs for latest version
    }
  }
}

resource "auth0_connection" "database" {
  name           = "Username-Password-Authentication-2"
  strategy       = "auth0"

  options {
    password_policy = "excellent"
    import_mode     = false
    custom_scripts = {
      get_user = <<EOF
        function getByEmail(email, callback) {
          return callback(new Error("Whoops!"));
        }
      EOF
    }
  }
}

output "database_connection_id" {
  description = "ID of the Auth0 database connection"
  value       = auth0_connection.database.id
}