terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0" # Refer to docs for latest version
    }
  }
}

resource "auth0_client" "webapp" {
  name            = var.app_name
  app_type        = "regular_web"
  callbacks       = var.callbacks
  oidc_conformant = true

  jwt_configuration {
    lifetime_in_seconds = 36000
    secret_encoded      = true
    alg                 = "RS256"
    scopes = {
      foo = "bar"
    }
  }

  grant_types = ["authorization_code", "client_credentials", "implicit", "refresh_token"]
}