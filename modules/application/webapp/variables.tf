variable "app_name" {
  description = "Name of the webapp"
  type        = string
}

variable "callbacks" {
  description = "Callback URLs for the webapp"
  type        = list(string)
}