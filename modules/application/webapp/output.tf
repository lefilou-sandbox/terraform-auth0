output "client_id" {
  description = "The client ID of the Auth0 webapp"
  value       = auth0_client.webapp.client_id
}

# output "client_secret" {
#   description = "The client secret of the Auth0 webapp"
#   value       = auth0_client.webapp.client_secret
# }