terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0" # Refer to docs for latest version
    }
  }
}

resource "auth0_client" "spa" {
  name            = var.app_name
  app_type        = "spa"
  callbacks       = var.callbacks
  oidc_conformant = true

  jwt_configuration {
    lifetime_in_seconds = 300
    secret_encoded      = true
    alg                 = "RS256"
    scopes = {
      foo = "bar"
    }
  }

  refresh_token {
    leeway          = 0
    token_lifetime  = 2592000
    rotation_type   = "rotating"
    expiration_type = "expiring"
  }

  grant_types = ["implicit", "authorization_code", "refresh_token"]

}

resource "auth0_connection_client" "database_assoc" {
  client_id     = auth0_client.spa.id
  connection_id = var.database_connection_id
}

resource "auth0_connection_client" "google_conn_assoc" {
  client_id     = auth0_client.spa.id
  connection_id = var.google_connection_id
}