output "client_id" {
  description = "The client ID of the Auth0 SPA application"
  value       = auth0_client.spa.client_id
}