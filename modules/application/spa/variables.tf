variable "app_name" {
  description = "Name of the SPA application"
  type        = string
}

variable "callbacks" {
  description = "Callback URLs for the SPA application"
  type        = list(string)
}

variable "database_connection_id" {
  description = "ID of the Auth0 database connection"
  type        = string
}

variable "google_connection_id" {
  description = "ID of the Auth0 Google connection"
  type        = string
}