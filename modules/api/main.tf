terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0"
    }
  }
}

resource "auth0_resource_server" "api" {
  name         = var.api_name
  identifier   = var.api_identifier
  signing_alg  = "RS256"

  allow_offline_access                            = true
  token_lifetime                                  = 8600
  skip_consent_for_verifiable_first_party_clients = true
}

resource "auth0_resource_server_scope" "read_data" {
  resource_server_identifier = auth0_resource_server.api.identifier
  scope                      = "read:data"
}

resource "auth0_resource_server_scope" "write_data" {
  resource_server_identifier = auth0_resource_server.api.identifier
  scope                      = "write:data"
}