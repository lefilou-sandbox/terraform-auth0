output "api_id" {
  description = "The ID of the Auth0 API"
  value       = auth0_resource_server.api.id
}