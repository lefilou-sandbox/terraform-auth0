variable "api_name" {
  description = "Name of the API"
  type        = string
}

variable "api_identifier" {
  description = "Identifier for the API"
  type        = string
}