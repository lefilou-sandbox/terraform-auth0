exports.onExecutePreUserRegistration = async (event, api) => {
    // Check if email domain is allowed
    const allowedDomains = ["example.com", "anotherexample.com"];
    const emailDomain = event.user.email.split("@")[1];
  
    if (!allowedDomains.includes(emailDomain)) {
      api.access.deny("Unauthorized email domain");
    }
  
    // Set initial app metadata
    api.user.setAppMetadata("roles", ["user"]);
  };