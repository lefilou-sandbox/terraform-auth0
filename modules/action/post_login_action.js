exports.onExecutePostLogin = async (event, api) => {
    const axios = require('axios');
  
    // Add custom claims to the ID token
    api.idToken.setCustomClaim("https://example.com/roles", event.user.app_metadata.roles);
  
    // Integrate with external service
    await axios.post('https://external-service.example.com/logins', {
      user_id: event.user.user_id,
      email: event.user.email,
      login_time: new Date().toISOString()
    });
  };