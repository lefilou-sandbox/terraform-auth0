terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = ">= 1.0.0"
    }
  }
}

resource "auth0_action" "post_login_action" {
  name     = "PostLoginAction"
  deploy   = true
  runtime  = "node16"

  supported_triggers {
    id = "post-login"
    version = "v3"
  }
  code = file("${path.module}/post_login_action.js")

  dependencies {
    name    = "axios"
    version = "0.21.1"
  }
}

resource "auth0_action" "pre_user_registration_action" {
  name     = "PreUserRegistrationAction"
  deploy   = true
  runtime  = "node16"
  supported_triggers {
    id = "pre-user-registration"
    version = "v2"
  }
  code = file("${path.module}/pre_user_registration_action.js")
}


output "post_login_action_id" {
  value = auth0_action.post_login_action.id
}

output "pre_user_registration_action_id" {
  value = auth0_action.pre_user_registration_action.id
}